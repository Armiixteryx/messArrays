# messArrays
Mess up arrays. It takes an array and returns it disordered.
Made in C language.

## Compile
To compile:
```
gcc -c messarray.c -o messarray.o
gcc -c sample.c -o sample.o
```

And for link files:
```
gcc sample.o messyarray.o -o sample.out
```

Execute it:
```
./sample.out
```
