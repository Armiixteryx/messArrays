#include <stdio.h>
#include "messarray.h"

int main(int argc, char **argv)
{
        unsigned int i = 0;
        
        int array_test[10] = {0,1,2,3,4,5,6,7,8,9};
        
        printf("The original array is: \n");
        for(i=0; i<10; i++)
                printf("%i ", array_test[i]);
        
        printf("\n");
        
        messarray(array_test, 10);
        
        printf("The messed up array is: \n");
        for(i=0; i<10; i++)
                printf("%i ", array_test[i]);
        
        printf("\n");
        
        return 0;
}
