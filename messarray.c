#include "messarray.h"

void messarray(int *array, const unsigned int size_array)
{
        unsigned int i = 0;
        int new_position = 0;
        int old_position = 0;
        
        srand((unsigned)time(NULL));
        while(i < size_array)
        {
                new_position = rand()%size_array;
                old_position = array[i];
                array[i] = array[new_position];
                array[new_position] = old_position;
                i++;
        }
}
